DOCKER_REGISTRY?=registry.gitlab.com
DOCKER_IMG_NAME?=media-cloud-ai/workers/media/rs-hls-playlist-maker-worker
ifneq ($(DOCKER_REGISTRY), )
	DOCKER_IMG_NAME := /${DOCKER_IMG_NAME}
endif

docker = docker run -v ${PWD}:/sources -v ${DATA_FOLDER}:/sources/data --rm ${DOCKER_REGISTRY}${DOCKER_IMG_NAME}:${VERSION}

docker-build:
	@docker build -t ${DOCKER_REGISTRY}${DOCKER_IMG_NAME}:${VERSION} .

docker-clean:
	@docker rmi ${DOCKER_REGISTRY}${DOCKER_IMG_NAME}:${VERSION}

docker-registry-login:
	@docker login --username "${CI_REGISTRY_USER}" -p ${CI_REGISTRY_PASSWORD} ${DOCKER_REGISTRY}

docker-push-registry:
	@docker push ${DOCKER_REGISTRY}${DOCKER_IMG_NAME}:${VERSION}

deploy:
	@$(eval AWX_TOKEN=$(awx login --conf.host $AWX_HOST --conf.username $AWX_USER --conf.password $AWX_PWD | jq -r '.token'))
	@awx --conf.host "${AWX_HOST}" --conf.token "${AWX_TOKEN}" -f human job_templates launch "HLS Playlist Maker - ${TARGET_ENVIRONMENT} - Deploy" --monitor --filter status --extra_vars "version_to_deploy: ${VERSION}"

check-version:
	@$(eval code := $(shell export DOCKER_CLI_EXPERIMENTAL=enabled; \
		docker manifest inspect ${DOCKER_REGISTRY}${DOCKER_IMG_NAME}:${VERSION} > /dev/null \
		&& echo 0 || echo 1))
	@if [ "${code}" = "0" ]; then \
		echo "image ${DOCKER_REGISTRY}${DOCKER_IMG_NAME}:${VERSION} already exists."; exit 1;\
	else \
		echo "image ${DOCKER_REGISTRY}${DOCKER_IMG_NAME}:${VERSION} is available."; \
	fi
