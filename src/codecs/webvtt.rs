use regex::Regex;
use std::fmt;
use std::str::FromStr;

#[derive(Copy, Clone, Debug)]
pub struct Timecode {
  pub hours: i64,
  pub minutes: i64,
  pub seconds: i64,
  pub milliseconds: i64,
}

impl Timecode {
  pub fn to_milliseconds(self) -> i64 {
    self.hours * 3_600_000 + self.minutes * 60_000 + self.seconds * 1000 + self.milliseconds
  }
}

pub fn from_milliseconds(ms: i64) -> Timecode {
  let seconds_f = ms as f64 / 1000.0;
  let (s_f, ms_f): (f64, f64) = split_int_frac(seconds_f);
  let frames_count: i64 = s_f as i64;
  let milliseconds: i64 = (ms_f * 1000.0).round() as i64;

  let rem_seconds: i64 = div_rem(frames_count, 60);
  let seconds_count: i64 = (frames_count - rem_seconds) / 60;
  let seconds: i64 = rem_seconds;

  let rem_minutes: i64 = div_rem(seconds_count, 60);
  let minutes_count: i64 = (seconds_count - rem_minutes) / 60;
  let minutes: i64 = rem_minutes;

  let rem_hours: i64 = div_rem(minutes_count, 60);
  let hours: i64 = rem_hours;

  Timecode {
    hours,
    minutes,
    seconds,
    milliseconds,
  }
}

fn split_int_frac(x: f64) -> (f64, f64) {
  let i = x.floor();
  let f = x - i;
  (i, f)
}

fn div_rem(x: i64, y: i64) -> i64 {
  x % y
}

impl FromStr for Timecode {
  type Err = ();
  fn from_str(input: &str) -> Result<Self, Self::Err> {
    let re = Regex::new(r"([0-9]{2}):([0-9]{2}):([0-9]{2}).([0-9]{3})").unwrap();
    let caps = re.captures(input).unwrap();

    let hours: i64 = caps
      .get(1)
      .map_or("", |m| m.as_str())
      .parse::<i64>()
      .unwrap();
    let minutes: i64 = caps
      .get(2)
      .map_or("", |m| m.as_str())
      .parse::<i64>()
      .unwrap();
    let seconds: i64 = caps
      .get(3)
      .map_or("", |m| m.as_str())
      .parse::<i64>()
      .unwrap();
    let milliseconds: i64 = caps
      .get(4)
      .map_or("", |m| m.as_str())
      .parse::<i64>()
      .unwrap();

    Ok(Timecode {
      hours,
      minutes,
      seconds,
      milliseconds,
    })
  }
}

impl fmt::Display for Timecode {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(
      f,
      "{:0>2}:{:0>2}:{:0>2}.{:0>3}",
      self.hours, self.minutes, self.seconds, self.milliseconds
    )
  }
}

#[derive(Clone, Debug)]
pub struct WebVtt {
  pub id: i64,
  pub begin: Timecode,
  pub end: Timecode,
  pub content: Option<String>,
}

pub fn webvtt_vec_from_string(input: String) -> Vec<WebVtt> {
  let mut vec: Vec<WebVtt> = vec![];
  let re =
    Regex::new(r"([0-9][0-9]*)\n([0-9:\.]{12}) --> ([0-9:\.]{12})\n(.*\n.*\n[a-zA-Z].*|.*\n.*|.*)")
      .unwrap();

  for caps in re.captures_iter(&input) {
    let id: i64 = caps
      .get(1)
      .map_or("", |m| m.as_str())
      .parse::<i64>()
      .unwrap();
    let begin: Timecode = caps
      .get(2)
      .map_or("", |m| m.as_str())
      .parse::<Timecode>()
      .unwrap();
    let end: Timecode = caps
      .get(3)
      .map_or("", |m| m.as_str())
      .parse::<Timecode>()
      .unwrap();
    let content: String = caps.get(4).map_or("", |m| m.as_str()).to_string();

    let part = WebVtt {
      id,
      begin,
      end,
      content: Some(content),
    };
    vec.push(part);
  }
  vec
}

pub fn webvtt_vec_to_string(input: Vec<WebVtt>) -> String {
  let mut stringified: String = "WEBVTT".to_string();

  for part in input {
    let content: String = if let Some(stripped) = part.content.as_ref().unwrap().strip_suffix("\n") {
      stripped.to_string()
    } else {
      part.content.unwrap()
    };
    stringified = format!(
      "{}\n\n{}\n{} --> {}\n{}",
      stringified,
      part.id,
      part.begin.to_string(),
      part.end.to_string(),
      content
    );
  }

  stringified
}
