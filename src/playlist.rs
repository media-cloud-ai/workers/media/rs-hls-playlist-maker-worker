use mcai_worker_sdk::{
  media::webvtt::Timecode,
  prelude::webvtt::{webvtt_vec_to_string, WebVtt},
  prelude::*,
};

use crate::tools::from_milliseconds;

use chrono::{DateTime, Duration, FixedOffset, Local};
use regex::Regex;
use std::fs;
use std::fs::{File, OpenOptions};
use std::io::prelude::*;
use std::io::Result;
use std::process::Command;

#[derive(Clone, Debug, Default)]
pub struct PlaylistMaker {
  pub clock: Option<DateTime<FixedOffset>>,
  pub last_end_timecode: i64,
  pub endpoint: String,
  pub first_index: i64,
  pub first_begin_timecode: i64,
  pub index: i64,
  pub languages: Vec<Language>,
  pub master_playlist: String,
  pub master_playlist_name: String,
  pub path: String,
  pub playlist: String,
  pub playlist_name: String,
  pub video_playlist: String,
}

#[derive(Clone, Debug, Default)]
pub struct Language {
  pub long: String,
  pub short: String,
}

pub fn update_master_playlist(config: &mut PlaylistMaker) -> Result<()> {
  // Get Master playlist from MediaStore
  let _output = Command::new("bash")
    .arg("-c")
    .arg(format!(
      "aws mediastore-data get-object --endpoint {} --path {}.m3u8 {}.m3u8",
      config.endpoint, config.master_playlist_name, config.master_playlist
    ))
    .output()
    .expect("failed to execute process");

  let master_playlist: String = format!("{}.m3u8", config.master_playlist.clone());

  let contents = fs::read_to_string(master_playlist.clone())
    .expect("Something went wrong reading the master playlist.");

  let re = Regex::new(r"([a-z0-9_]*\.m3u8)").unwrap();
  let caps = re.captures(&contents).unwrap();
  config.video_playlist = caps.get(1).map_or("", |m| m.as_str()).to_string();

  let mut lines: Vec<String> = contents.split('\n').map(|s| s.to_string()).collect();

  lines.pop();

  let mut subs: bool = false;
  let mut count = 0;

  for line in lines.iter_mut() {
    if line.contains("#EXT-X-STREAM-INF:") && !line.contains("SUBTITLES") {
      *line += ",SUBTITLES=\"subs\"";
    } else if line.contains("#EXT-X-MEDIA:TYPE=SUBTITLES") {
      subs = true;
      count += 1;
    }
  }

  if subs {
    for _ in 0..count {
      lines.pop();
    }
  }

  if !config.video_playlist.is_empty() {
    // Get Master playlist from MediaStore
    let _output = Command::new("bash")
      .arg("-c")
      .arg(format!(
        "aws mediastore-data get-object --endpoint {} --path {}/{} {}",
        config.endpoint, config.path, config.video_playlist, config.video_playlist
      ))
      .output()
      .expect("failed to execute process");

    let contents = fs::read_to_string(config.video_playlist.clone())
      .expect("Something went wrong reading the video playlist.");

    let re = Regex::new(r"#EXT-X-MEDIA-SEQUENCE:([0-9]*)").unwrap();
    let caps = re.captures(&contents).unwrap();
    if let Ok(index) = caps.get(1).map_or("", |m| m.as_str()).parse::<i64>() {
      info!("Index from playlist: {}", index);
      config.index = index + 9;
      config.first_index = index;
    }

    let re = Regex::new(r"#EXT-X-PROGRAM-DATE-TIME:([0-9-TZ:\.]{24})").unwrap();
    let caps = re.captures(&contents).unwrap();
    let clock_s = caps.get(1).map_or("", |m| m.as_str());
    info!("Clock from playlist: {}", clock_s);
    let clock = DateTime::parse_from_rfc3339(clock_s).unwrap();
    config.clock = Some(clock);
  } else {
    error!("No video playlist in master playlist.")
  }

  for lang in &config.languages {
    let subtitle_config: String = format!("#EXT-X-MEDIA:TYPE=SUBTITLES,GROUP-ID=\"subs\",NAME=\"{}\",DEFAULT=YES,AUTOSELECT=YES,FORCED=NO,LANGUAGE=\"{}\",CHARACTERISTICS=\"public.accessibility.transcribes-spoken-dialog\",URI=\"subtitles/{}_webvtt_{}.m3u8\"", lang.long, lang.short, config.playlist, lang.short);
    lines.push(subtitle_config);
  }

  let playlist_content: String = lines.join("\n");

  let mut file = OpenOptions::new()
    .write(true)
    .truncate(true)
    .open(master_playlist)
    .unwrap();

  if let Err(e) = writeln!(file, "{}", playlist_content) {
    error!("Couldn't write to file: {}", e);
  }

  // Copy to MediaStore
  let _output = Command::new("bash")
                .arg("-c")
                .arg(format!(
                  "aws mediastore-data put-object --endpoint {} --body {}.m3u8 --path {}.m3u8 --cache-control \"max-age=6, public\" --content-type binary/octet-stream --region eu-west-1",
                  config.endpoint,
                  config.master_playlist,
                  config.master_playlist_name
                ))
                .output()
                .expect("failed to execute process");

  Ok(())
}

pub fn create_first_playlist(config: &PlaylistMaker) -> Result<()> {
  let local_time = Local::now();
  let headers: String =
    format!("#EXTM3U\n#EXT-X-VERSION:4\n#EXT-X-TARGETDURATION:10\n#EXT-X-MEDIA-SEQUENCE:{}\n#EXT-X-PROGRAM-DATE-TIME:{}\n", config.index - 9, local_time.format("%Y-%m-%dT%H:%M:%S.%3fZ"));

  for lang in &config.languages {
    let mut playlist_file = File::create(format!(
      "{}_{}.m3u8",
      config.playlist_name.clone(),
      lang.short
    ))?;
    match playlist_file.write(headers.as_bytes()) {
      Err(_) => error!("Could not write in file"),
      _ => debug!("Written to file"),
    };
    drop(playlist_file);
  }

  Ok(())
}

#[allow(clippy::assign_op_pattern)]
pub fn create_playlist(
  p_webvtt_vec: Vec<WebVtt>,
  config: &mut PlaylistMaker,
  iteration: i64,
) -> Result<()> {
  if !p_webvtt_vec.is_empty() {
    let mut webvtt_vec: Vec<WebVtt> = p_webvtt_vec;

    if iteration < 10 {
      if let Err(e) = update_master_playlist(config) {
        error!("{}", format!("Cannot update master playlist: {}", e));
      }
    }

    info!("Input: {:?}", webvtt_vec);
    let re = Regex::new(r"CLOCK:([0-9-TZ:\.]{26,29}\+[0-9:]{5})").unwrap();
    let first_webvtt: WebVtt = webvtt_vec[0].clone();
    let headers: Vec<String> = first_webvtt.headers.unwrap();
    let clock = if !headers.is_empty() {
      let header: String = headers[0].clone(); // Can it ever crash ?
      if let Some(caps) = re.captures(&header) {
        let clock_s = caps.get(1).map_or("", |m| m.as_str());
        info!("Clock from transcript: {}", clock_s);
        DateTime::parse_from_rfc3339(clock_s).unwrap()
      } else {
        config.clock.unwrap() + Duration::seconds(10 * (config.index - config.first_index))
      }
    } else {
      config.clock.unwrap() + Duration::seconds(10 * (config.index - config.first_index))
    };
    let mut ref_clock =
      config.clock.unwrap() + Duration::seconds(10 * (config.index - config.first_index));

    info!("Clock from reference: {}", ref_clock);
    let mut delta = (clock - ref_clock).num_milliseconds();
    while delta + 3000 < 0 {
      ref_clock = ref_clock - Duration::seconds(10);
      config.index -= 1;
      delta = (clock - ref_clock).num_milliseconds();
    }

    let nth_vec_length = webvtt_vec.len() / config.languages.len();

    webvtt_vec = adapt_timecodes_webvtt(&mut webvtt_vec, config, delta, nth_vec_length)?;

    if !webvtt_vec.is_empty() {
      config.first_begin_timecode = webvtt_vec.first().unwrap().begin.to_milliseconds();
      config.last_end_timecode = webvtt_vec.last().unwrap().end.to_milliseconds();
    }

    for (lang_id, lang) in config.languages.iter().enumerate() {
      let n = nth_vec_length * lang_id;
      let p = nth_vec_length * (lang_id + 1);
      let partial_webvtt_vec = &webvtt_vec[n..p];

      let result = webvtt_vec_to_string(partial_webvtt_vec.to_vec(), false, false);

      if let Err(e) = &result {
        error!("Error: {}", e);
      }

      let mut text = result.unwrap();

      text = text.replace(
        "WEBVTT",
        &format!(
          "WEBVTT\nX-TIMESTAMP-MAP=MPEGTS:{},LOCAL:00:00:00.000",
          90000 * ((config.index - 1) * 10 + 2)
        )[..],
      );

      info!("{}", text);

      let chunk_filename = format!(
        "{}/subtitles/{}_{}_{:0>5}.webvtt",
        config.path, config.playlist, lang.short, config.index
      );

      let mut file = File::create(chunk_filename.clone())?;

      match file.write(text.as_bytes()) {
        Err(_) => error!("Could not write in file"),
        _ => debug!("Written to file"),
      };

      // Copy to MediaStore
      let _output = Command::new("bash")
                  .arg("-c")
                  .arg(format!(
                    "aws mediastore-data put-object --endpoint {} --body {} --path {} --cache-control \"max-age=6, public\" --content-type binary/octet-stream --region eu-west-1",
                    config.endpoint,
                    chunk_filename,
                    chunk_filename,
                  ))
                  .output()
                  .expect("failed to execute process");

      fs::remove_file(chunk_filename)?;

      let start_index = if config.index > 9 {
        config.index - 9
      } else {
        0
      };

      let timestamp: String = if !config.video_playlist.is_empty() {
        let _output = Command::new("bash")
          .arg("-c")
          .arg(format!(
            "aws mediastore-data get-object --endpoint {} --path {}/{} {}",
            config.endpoint, config.path, config.video_playlist, config.video_playlist
          ))
          .output()
          .expect("failed to execute process");

        let contents = fs::read_to_string(config.video_playlist.clone())
          .expect("Something went wrong reading the video playlist.");

        let re = Regex::new(r"#EXT-X-PROGRAM-DATE-TIME:([0-9T:Z\-\.]*)").unwrap();
        let caps = re.captures(&contents).unwrap();
        let local_time = caps.get(1).map_or("", |m| m.as_str()).to_string();
        info!("Local Time from playlist: {}", local_time);
        local_time
      } else {
        let local_time = Local::now();
        local_time.format("%Y-%m-%dT%H:%M:%S.%3fZ").to_string()
      };

      let mut playlist_content = format!(
        "#EXTM3U\n#EXT-X-VERSION:4\n#EXT-X-TARGETDURATION:10\n#EXT-X-MEDIA-SEQUENCE:{}\n#EXT-X-PROGRAM-DATE-TIME:{}\n",
        start_index,
        timestamp
      );

      info!("{}", playlist_content);

      for i in start_index..config.index + 1 {
        let chunk_ref = format!("{}_{}_{:0>5}.webvtt", config.playlist, lang.short, i);
        playlist_content = format!("{}#EXTINF:10.000000,\n{}\n", playlist_content, chunk_ref);
      }

      if let Err(err) = write_playlist(
        format!("{}_{}.m3u8", config.playlist_name.clone(), lang.short),
        playlist_content,
      ) {
        error!("{:?}", err);
      }

      // Copy to MediaStore
      let _output = Command::new("bash")
                .arg("-c")
                .arg(format!(
                  "aws mediastore-data put-object --endpoint {} --body {}_{}.m3u8 --path {}_{}.m3u8 --cache-control \"max-age=6, public\" --content-type binary/octet-stream --region eu-west-1",
                  config.endpoint,
                  config.playlist_name, lang.short,
                  config.playlist_name, lang.short
                ))
                .output()
                .expect("failed to execute process");
    }
    config.index += 1;
  }

  Ok(())
}

pub fn write_playlist(playlist_name: String, playlist_content: String) -> Result<()> {
  let mut playlist = OpenOptions::new()
    .write(true)
    .truncate(true)
    .open(playlist_name)?;
  match playlist.write(playlist_content.as_bytes()) {
    Err(_) => error!("Could not write in file"),
    _ => debug!("Written to file"),
  };
  drop(playlist);

  Ok(())
}

fn adapt_timecodes_webvtt(
  webvtt_vec: &mut [WebVtt],
  config: &mut PlaylistMaker,
  delta: i64,
  nth_vec_length: usize,
) -> Result<Vec<WebVtt>> {
  let neg_delta = delta < 0;
  let mut last_timestamp_end_time: Option<Timecode> = None;
  for (i, webvtt) in webvtt_vec.iter_mut().enumerate() {
    let mut begin = webvtt.begin.to_milliseconds();
    let mut end = webvtt.end.to_milliseconds();
    if i == 0 || i == nth_vec_length {
      begin = config.last_end_timecode
        - 10_000
        - (10_000 - (config.last_end_timecode - config.first_begin_timecode));
      if !neg_delta {
        end += delta;
      }
    } else if !neg_delta {
      begin += delta;
      end += delta;
    }
    begin = std::cmp::max(begin, 0);
    // TODO : There is a better way to do this, with a more general algortihm
    // pushing the first timestamp end (0e) by the absolute diff between 0e and the first
    // timestamp beginning (0b) plus the 0b, and to iterate through the rest of timestamps so
    // that the inital delta time between timestamps is still respected.
    // This quick solution is just to avoid block superpositions.
    if begin > end {
      webvtt.begin = from_milliseconds(end);
      webvtt.end = from_milliseconds(begin);
    } else {
      webvtt.begin = from_milliseconds(begin);
      webvtt.end = from_milliseconds(end);
    }
    if let Some(last_timestamp_end_time) = last_timestamp_end_time {
      if last_timestamp_end_time.to_milliseconds() > webvtt.begin.to_milliseconds() {
        webvtt.begin = last_timestamp_end_time;
      }
    }
    last_timestamp_end_time = Some(webvtt.end);
  }
  Ok(webvtt_vec.to_vec())
}

#[test]
#[allow(clippy::assign_op_pattern)]
fn test_adapt_timecodes_webvtt_begin_later_than_end() {
  let clock = DateTime::parse_from_rfc3339("2022-11-04T15:49:17.048388630+00:00").unwrap();
  let mut ref_clock = DateTime::parse_from_rfc3339("2022-11-04T15:49:00.090+00:00").unwrap();
  let _delta = (clock - ref_clock).num_milliseconds();

  let mut config = PlaylistMaker {
    clock: None,
    last_end_timecode: 30_562,
    first_begin_timecode: 23_969,
    endpoint: "".to_string(),
    first_index: 79,
    index: 88,
    languages: vec![],
    master_playlist: "".to_string(),
    master_playlist_name: "".to_string(),
    path: "".to_string(),
    playlist: "".to_string(),
    playlist_name: "".to_string(),
    video_playlist: "".to_string(),
  };

  let mut delta = (clock - ref_clock).num_milliseconds();
  while delta + 3000 < 0 {
    ref_clock = ref_clock - Duration::seconds(10);
    config.index -= 1;
    delta = (clock - ref_clock).num_milliseconds();
  }

  let mut vec_webvtt = vec![
    WebVtt {
      id: 320,
      begin: mcai_worker_sdk::media::webvtt::Timecode {
        hours: 0,
        minutes: 0,
        seconds: 0,
        milliseconds: 0,
      },
      end: mcai_worker_sdk::media::webvtt::Timecode {
        hours: 0,
        minutes: 0,
        seconds: 2,
        milliseconds: 399,
      },
      content: Some("sommes quasiment à 60 heures \n du départ de".to_string()),
      headers: Some(vec!["CLOCK:2022-11-04T15:49:17.048388630+00:00".to_string()]),
    },
    WebVtt {
      id: 321,
      begin: mcai_worker_sdk::media::webvtt::Timecode {
        hours: 0,
        minutes: 0,
        seconds: 2,
        milliseconds: 399,
      },
      end: mcai_worker_sdk::media::webvtt::Timecode {
        hours: 0,
        minutes: 0,
        seconds: 4,
        milliseconds: 279,
      },
      content: Some("la Route du Rhum.\n - Mais à la marina".to_string()),
      headers: Some(vec!["CLOCK:2022-11-04T15:49:17.048388630+00:00".to_string()]),
    },
    WebVtt {
      id: 322,
      begin: mcai_worker_sdk::media::webvtt::Timecode {
        hours: 0,
        minutes: 0,
        seconds: 4,
        milliseconds: 279,
      },
      end: mcai_worker_sdk::media::webvtt::Timecode {
        hours: 0,
        minutes: 0,
        seconds: 7,
        milliseconds: 299,
      },
      content: Some("de Pointe à Pitre, \n tout s'organise déjà pour accueillir".to_string()),
      headers: Some(vec!["CLOCK:2022-11-04T15:49:17.048388630+00:00".to_string()]),
    },
    WebVtt {
      id: 323,
      begin: mcai_worker_sdk::media::webvtt::Timecode {
        hours: 0,
        minutes: 0,
        seconds: 7,
        milliseconds: 299,
      },
      end: mcai_worker_sdk::media::webvtt::Timecode {
        hours: 0,
        minutes: 0,
        seconds: 9,
        milliseconds: 879,
      },
      content: Some("les 138 skippeurs \n de cette nouvelle édition".to_string()),
      headers: Some(vec!["CLOCK:2022-11-04T15:49:17.048388630+00:00".to_string()]),
    },
  ];
  let nth_vec_length = vec_webvtt.len();
  info!(
    "WebVTT vec : {:#?}",
    adapt_timecodes_webvtt(&mut vec_webvtt, &mut config, delta, nth_vec_length),
  );
}
