#[macro_use]
extern crate serde_derive;

mod playlist;
mod tools;

use mcai_worker_sdk::{
  default_rust_mcai_worker_description, job::JobResult, prelude::webvtt::WebVtt, prelude::*,
  MessageError,
};
use playlist::{
  create_first_playlist, create_playlist, update_master_playlist, Language, PlaylistMaker,
};
use std::str::FromStr;
use std::sync::mpsc::Sender;
use std::sync::{Arc, Mutex};

#[derive(Debug, Default)]
struct PlaylistMakerEvent {
  playlist_maker: PlaylistMaker,
  iteration: i64,
}

#[derive(Clone, Debug, Default, Deserialize, JsonSchema, PartialEq, Eq)]
pub enum InputDataFormat {
  #[default]
  EbuTtmlLive,
  WebVtt,
}

impl FromStr for InputDataFormat {
  type Err = MessageError;

  fn from_str(input: &str) -> Result<InputDataFormat> {
    match input {
      "EbuTtmlLive" => Ok(InputDataFormat::EbuTtmlLive),
      "WebVtt" => Ok(InputDataFormat::WebVtt),
      _ => Err(MessageError::RuntimeError(format!(
        "Invalid Enum: {:?}",
        input
      ))),
    }
  }
}

#[derive(Debug, Clone, Deserialize, JsonSchema)]
#[allow(dead_code)]
pub struct WorkerParameters {
  /// # Format
  /// Input data format
  format: Option<InputDataFormat>,
  /// # Master Playlist
  /// Master Playlist name
  master_playlist: Option<String>,
  /// # Path
  /// Path to playlist
  path: Option<String>,
  /// # Playlist
  /// Playlist name
  playlist: Option<String>,
  /// # Endpoint
  /// Endpoint url
  endpoint: Option<String>,
  /// # Languages short
  /// Languaes name short (fr, en, etc.)
  languages_short: Vec<String>,
  /// # Languages long
  /// Languaes name long (French, English, etc.) for player
  languages_long: Vec<String>,
  destination_path: String,
  source_path: String,
}

default_rust_mcai_worker_description!();

impl McaiWorker<WorkerParameters, RustMcaiWorkerDescription> for PlaylistMakerEvent {
  fn init_process(
    &mut self,
    parameters: WorkerParameters,
    _format_context: Arc<Mutex<FormatContext>>,
    _response_sender: Arc<Mutex<Sender<ProcessResult>>>,
  ) -> Result<Vec<StreamDescriptor>> {
    let input_data = parameters.format.unwrap_or_default();
    let endpoint = parameters.endpoint.unwrap_or_default();
    let path = parameters.path.unwrap_or_else(|| ".".to_string());
    let master_playlist = parameters
      .master_playlist
      .unwrap_or_else(|| "master".to_string());
    let master_playlist_name = format!("{}/{}", path, master_playlist);
    let playlist = parameters.playlist.unwrap_or_else(|| "master".to_string());
    let playlist_name = format!("{}/subtitles/{}_webvtt", path, playlist);
    let langs_short = parameters.languages_short;
    let langs_long = parameters.languages_long;
    let mut languages: Vec<Language> = vec![];
    for (id, _) in langs_short.iter().enumerate() {
      languages.push(Language {
        short: langs_short[id].clone(),
        long: langs_long[id].clone(),
      });
    }

    self.playlist_maker = PlaylistMaker {
      clock: None,
      last_end_timecode: 0,
      endpoint,
      first_index: 0,
      first_begin_timecode: 0,
      index: 0,
      languages,
      master_playlist,
      master_playlist_name,
      path,
      playlist,
      playlist_name,
      video_playlist: "".to_string(),
    };

    self.iteration = 0;

    if let Err(e) = create_first_playlist(&self.playlist_maker.clone()) {
      error!("{}", format!("Cannot create first playlist: {}", e));
    }

    if let Err(e) = update_master_playlist(&mut self.playlist_maker) {
      error!("{}", format!("Cannot update master playlist: {}", e));
    }

    match input_data {
      InputDataFormat::EbuTtmlLive => Ok(vec![StreamDescriptor::new_ebu_ttml_live(0)]),
      InputDataFormat::WebVtt => Ok(vec![StreamDescriptor::new_webvtt(0)]),
    }
  }

  fn process_frames(
    &mut self,
    _job_result: JobResult,
    _stream_index: usize,
    process_frames: &[ProcessFrame],
  ) -> Result<ProcessResult> {
    // We don't use batch processing
    let process_frame = &process_frames[0];
    if let ProcessFrame::WebVtt(frame) = &process_frame {
      debug!("{:?}", frame);
      let webvtt_vec: Vec<WebVtt> = (**frame).clone();
      if let Err(e) = create_playlist(webvtt_vec, &mut self.playlist_maker, self.iteration) {
        error!("{}", format!("Cannot create playlist: {}", e));
      }
      self.iteration += 1;
    }
    Ok(ProcessResult::empty())
  }

  fn ending_process(&mut self) -> Result<()> {
    Ok(())
  }
}

#[cfg(not(tarpaulin_include))]
fn main() {
  let worker = PlaylistMakerEvent::default();
  start_worker(worker);
}
