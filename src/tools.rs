use mcai_worker_sdk::media::webvtt::Timecode;

pub fn from_milliseconds(ms: i64) -> Timecode {
  let seconds_f = ms as f64 / 1000.0;
  let (s_f, ms_f): (f64, f64) = split_int_frac(seconds_f);
  let frames_count: i64 = s_f as i64;
  let milliseconds: i64 = (ms_f * 1000.0).round() as i64;

  let rem_seconds: i64 = div_rem(frames_count, 60);
  let seconds_count: i64 = (frames_count - rem_seconds) / 60;
  let seconds: i64 = rem_seconds;

  let rem_minutes: i64 = div_rem(seconds_count, 60);
  let minutes_count: i64 = (seconds_count - rem_minutes) / 60;
  let minutes: i64 = rem_minutes;

  let rem_hours: i64 = div_rem(minutes_count, 60);
  let hours: i64 = rem_hours;

  Timecode {
    hours,
    minutes,
    seconds,
    milliseconds,
  }
}

fn split_int_frac(x: f64) -> (f64, f64) {
  let i = x.floor();
  let f = x - i;
  (i, f)
}

fn div_rem(x: i64, y: i64) -> i64 {
  x % y
}
