FROM ubuntu:focal as builder
ENV TZ=Europe/Paris

ADD . /src
WORKDIR /src

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone && \
    apt-get update && \
    apt-get install -y \
    clang \
    curl \
    gcc \
    llvm \
    libavcodec-dev \
    libavdevice-dev \
    libavfilter-dev \
    libavformat-dev \
    libavresample-dev \
    libavutil-dev \
    libasound2-dev \
    libclang1 \
    libssl-dev \
    pkg-config \
    && \
    curl https://sh.rustup.rs -sSf | \
    sh -s -- --default-toolchain stable -y && \
    . $HOME/.cargo/env && \
    cargo build --verbose --release && \
    cargo install --path .

FROM ubuntu:focal
COPY --from=builder /root/.cargo/bin/rs_hls_playlist_maker_worker /usr/bin

RUN mkdir -p /franceinfo && \
    mkdir -p /franceinfo/subtitles

RUN apt update && \
    apt install -y \
    ca-certificates \
    curl \
    libavcodec58 \
    libavdevice58 \
    libavfilter7 \
    libavformat58 \
    libavresample4 \
    libavutil56 \
    libssl1.1 \
    unzip


RUN curl -s "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    rm -rf ./aws awscliv2.zip

ENV AMQP_QUEUE job_hls_playlist_maker
CMD rs_hls_playlist_maker_worker
